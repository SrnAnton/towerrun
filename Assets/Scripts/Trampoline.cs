﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    [SerializeField] private float _additionalJumpForce;
    [SerializeField] private int _multiplier;

    public float GetAdditionalForce()
    {
        return _additionalJumpForce * _multiplier;
    }

    public void SetMultiplier(int factor)
    {
        _multiplier = factor;
    }
}
