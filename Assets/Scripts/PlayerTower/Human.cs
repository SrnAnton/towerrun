﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Human : MonoBehaviour
{
    [SerializeField] private Transform  _fixationPoint;
    [SerializeField] private float  _bounceForce;

    public Transform FixationPoint => _fixationPoint;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Waving(bool state)
    {
        _animator.SetBool("isWaving", true);
    }

    public void Run(bool state)
    {
        _animator.SetBool("isRunning", state);
    }

    public void Bounce()
    {
      /*  Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
        if(rigidbody != null)
        {
            float radius = Random.Range(0.0f, 360.0f);
            rigidbody.isKinematic = false;
            rigidbody.AddExplosionForce(_bounceForce, transform.position, 50.0f);
        }    */
    }
}
