﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTower : MonoBehaviour
{
    [SerializeField] private Human _startHuman;
    [SerializeField] private Transform _distanceChecker;
    [SerializeField] private float _fixationMaxDistance;
    [SerializeField] private BoxCollider _checkCollider;
    [SerializeField] private float _displaceScale = 1.5f;

    private List<Human> _humans;

    public event UnityAction<int> HumanAdded;

    private void Start()
    {
        _humans = new List<Human>();
        Vector3 spawnPoint = transform.position;
        _humans.Add(Instantiate(_startHuman, spawnPoint, Quaternion.identity, transform));
        _humans[0].Run(true);

        HumanAdded?.Invoke(_humans.Count);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out Human human))
        {
            Tower collisionTower = human.GetComponentInParent<Tower>();
            if (collisionTower != null)
            {
                AddHumans(collisionTower.CollectHuman(_distanceChecker, _fixationMaxDistance));
                collisionTower.Break();
            }
        }
    }

    private void AddHumans(List<Human> collectedHumans)
    {
        if (collectedHumans != null)
        {
            _humans[0].Run(false);
            for (int i = collectedHumans.Count - 1; i >= 0; i--)
            {
                Human currentHuman = collectedHumans[i];
                InsertHuman(currentHuman);
                DisplaceCheckers(currentHuman);
                HumanAdded?.Invoke(_humans.Count);
            }
            _humans[0].Run(true);
        }
    }

    private void InsertHuman(Human human)
    {
        _humans.Insert(0, human);
        SetHumanPosition(human);
    }

    private void DisplaceCheckers(Human human)
    {
        Vector3 distanceCheckerNewPosition = _distanceChecker.position;
        distanceCheckerNewPosition.y -= human.transform.localScale.y * _displaceScale;
        _distanceChecker.position = distanceCheckerNewPosition;
        _checkCollider.center = _distanceChecker.localPosition;
    }

    private void SetHumanPosition(Human human)
    {
        human.transform.parent = transform;
        human.transform.localPosition = new Vector3(0.0f, human.transform.localPosition.y, 0);
        human.transform.localRotation = Quaternion.identity;
    }
}
