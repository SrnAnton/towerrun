﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanRandomSkin : MonoBehaviour
{
    [SerializeField] private GameObject _parentOfmodels;

    private void Start()
    {
        int randomChild = Random.Range(0, _parentOfmodels.transform.childCount);
        _parentOfmodels.transform.GetChild(randomChild).gameObject.SetActive(true);
    }
}
