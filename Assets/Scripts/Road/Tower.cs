﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private Human[] _humansTemplates;
    [SerializeField] private Vector2Int _humanInTowerRange;
    [SerializeField] private float _deathTime = 0.03f;

    private List<Human> _humanInTower;

    private void Start()
    {
        _humanInTower = new List<Human>();
        int humanInTowerCount = Random.Range(_humanInTowerRange.x, _humanInTowerRange.y);
        SpawnHumans(humanInTowerCount);
    }

    private void SpawnHumans(int humanCount)
    {
        Vector3 spawnPoint = transform.position;

        for (int i = 0; i < humanCount; i++)
        {
            int current = Random.Range(0, _humansTemplates.Length);
            Human spawnedHuman = _humansTemplates[current];
            _humanInTower.Add(Instantiate(spawnedHuman, spawnPoint, Quaternion.identity, transform));

            _humanInTower[i].transform.localPosition = new Vector3(0.0f, _humanInTower[i].transform.localPosition.y, 0.0f);

            spawnPoint = _humanInTower[i].FixationPoint.position;
        }
    }

    public List<Human> CollectHuman(Transform distanceChecker, float fixationMaxDistance)
    {
        for (int i = 0; i < _humanInTower.Count; i++)
        {
            float distanceBetweenPoints = GetVerticalDistance(distanceChecker, _humanInTower[i].FixationPoint.transform);
            
            if(distanceBetweenPoints < fixationMaxDistance)
            {
                List<Human> collectedHumans = _humanInTower.GetRange(0, i + 1);
                _humanInTower.RemoveRange(0, i + 1);
                return collectedHumans;
            } 
        }

        return null;
    }

    private float GetVerticalDistance(Transform distanceChecker, Transform humanFixationPoint)
    {
        Vector3 distanceCheckerY = new Vector3(0.0f, distanceChecker.position.y, 0.0f);
        Vector3 humanFixationPointY = new Vector3(0.0f, humanFixationPoint.position.y, 0.0f);
        return Vector3.Distance(distanceCheckerY, humanFixationPointY);
    }

    public void Break()
    {
        for(int i = 0; i < _humanInTower.Count; i++)
        {
            _humanInTower[i].Bounce();
        }

        Destroy(gameObject, _deathTime);
    }
}
